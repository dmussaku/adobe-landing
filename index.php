<?php 
if(isset($_POST['submit'])){
    $to = "adobe@v3na.com"; // this is your Email address
    $name = $_POST['first_name'];
    $from = $_POST['email']; // this is the sender's Email address
    $phone = $_POST['phone'];
    $message= $_POST['message'];
    $message_1 = $name . "\n\n " . $from . "\n\n" . $phone . "\n\n" . $message;

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    echo "Ваша заявка была принята!";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="shortcut icon" href="static/images/favicon.ico">
        
        <link rel="stylesheet" href="startup/flat-ui/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="startup/flat-ui/css/flat-ui.css">
        <!-- Using only with Flat-UI (free)-->
        <link rel="stylesheet" href="startup/common-files/css/icon-font.css">
        <!-- end -->
        <link rel="stylesheet" href="startup/common-files/css/animations.css">
        <link rel="stylesheet" href="static/css/style.css">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:600,700' rel='stylesheet' type='text/css'>
        <link href="http://fonts.googleapis.com/css?family=Righteous" rel="stylesheet" type="text/css">
        <!-- <link rel="stylesheet" href="static/css/countdown.demo.css" type="text/css"> -->

        <link rel="stylesheet" href="static/css/main.css">
        
        
        <!--
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
          -->
        <title>Adobe</title>
    </head>

    <body>
    <div id="fb-root"></div>
        <div class="page-wrapper"><header class="header-6">
    <div class="container">
        <div class="row">
            <div class="navbar col-sm-12" role="navigation" style="top:0px;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle"></button>
                    <!-- <a class="brand" href="#"><img src="startup/common-files/icons/Infinity-Loop@2x.png" width="50" height="50" alt=""> Adobe</a> -->
                    <a class="brand" href="#"><img src="static/images/adobe_up.png" width="70" height="118" alt="" style="margin-top:0px;"></a>
                    <div class="social-btns" style="margin-top:70px;">
                        <!-- <a href="http://www.vk.com">
                            <div class="fui-vimeo"></div>
                            <div class="fui-vimeo"></div>
                        </a> -->
                        <a href="https://www.facebook.com/almacloudkz">
                            <div class="fui-facebook"></div>
                            <div class="fui-facebook"></div>
                        </a>
                        <a href="https://twitter.com/AlmaCloud">
                            <div class="fui-twitter"></div>
                            <div class="fui-twitter"></div>
                        </a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" style="margin-top:70px;">
                    <ul class="nav">
                        <!-- <li><a href="#1">Lorem</a></li> -->
                        <li><a href="#2">Преимущества</a></li>
                        <li><a href="#3">Продукты</a></li>
                        <li><a href="#4">Цены</a></li>
                        <li><a href="#5">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="header-6-sub" id="1" >
    <div class="background">&nbsp;</div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3>Adobe Creative Cloud<br> Скидка 30% на три года</h3>
                <div class="row">
                    <p class="lead col-sm-10 col-sm-offset-1" style="color:white;">
                        Приобретите подписку до 31 августа 2014, получите скидку 30% и сохраните цену на три следующих года.
                    </p>
                    <div class="example example--vertical-flip" style="background-color:transparent;">
                            <div class="countdown">
                                <div class="unit-wrap">
                                    <div class="days"></div>
<!--                                     <span style="color:white;" class="ce-days-label"></span>
                                          -->
                                          <span style="color:white;">Дней</span>
                                </div>
                                <div class="unit-wrap">
                                    <div class="hours"></div>
<!--                                     <span style="color:white;" class="ce-hours-label"></span>
 -->                                <span style="color:white;">Часов</span>    
                                </div>
                                <div class="unit-wrap">
                                    <div class="minutes"></div>
<!--                                     <span style="color:white;" class="ce-minutes-label"></span>
 -->                                <span style="color:white;">Минут</span>    
                                </div>
                                <div class="unit-wrap">
                                    <div class="seconds"></div>
<!--                                     <span style="color:white;" class="ce-seconds-label"></span>
 -->                                    <span style="color:white;">Секунд</span>
                                    </div>
                            </div>
                        </div>
                </div>
                <!--Timer-->
                <div class="row">
                    <div class="container">     
                        
                    </div>
                </div>
                <!-- <div><time>2014-08-29T17:47:00+0100</time></div> -->
                <div class="centered">
                    <!-- <button class="btn btn-danger btn-primary" type="button" href="#3">Отправить Заявку</button> -->
                    <a class="btn btn-danger btn-primary" href="#5" style="font-size: 20px; padding: 22px 15px;">БЕСПЛАТНО 30 дней</a>
                </div>
                    <!-- <iframe id="pPlayer"f
                            src="http://player.vimeo.com/video/66882085?title=0&amp;byline=0&amp;portrait=0&amp;api=1&amp;player_id=pPlayer"
                            frameborder="0" width="500" height="334" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
                         
                
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="signup-form">
                    <form>
                        <div class="clearfix">
                            <input type="text" class="form-control" placeholder="Your E-mail">
                            <input type="password" class="form-control" placeholder="Password">
                            <input type="password" class="form-control" placeholder="Confirmation">
                            <button type="submit" class="btn btn-primary">Sign Up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> -->
    </div>
</section>
<section style="margin-top:100px; padding-top:30px;" class="content-5" id="2" style="margin-top:100px; padding-top:30px;">
    
    <div  class="container">
        <img data-scroll-reveal="after 0.33s, ease-in 32px and reset over .66s" src="static/images/cc.png" width="200" height="200" alt="">

        <div class="row">
            <div data-scroll-reveal="after 0.2s, ease-in 32px and reset over .66s" class="col-sm-10 col-sm-offset-1">
                <h3>Adobe Creative Cloud для рабочих групп.<br> Заменить на "Безграничные возможности для творчества </h3>

                <p class="lead">Преимущества Creative Cloud для рабочих групп</p>
            </div>
        </div>
        <div  class="row features">
            <div data-scroll-reveal="enter from the left after 0.2s" class="col-sm-4 col-sm-offset-2">
                <span class="fui-cmd"> </span>
                <h6>Инновации</h6>

                <p>Установите новейшие версии всех приложений Creative Cloud для настольных компьютеров, включая такие популярные приложения, как Adobe Photoshop, Illustrator и InDesign, приложения и сервисы для мобильных устройств. По мере появления новых функций вы сможете сразу же ими пользоваться. </p>
            </div>
            <div data-scroll-reveal="enter from the right after 0.2s" class="col-sm-4 col-sm-offset-1">
                <span class="fui-windows"> </span>
                <h6>Совместная работа</h6>

                <p>Облачное хранилище объемом 100 ГБ, функции совместного использования файлов, управление версиями и централизованный сбор данных обо всех операциях по проектам — это и многое другое повысит эффективность вашей работы.</p>
            </div>
        </div>
        <div class="row features">
            <div data-scroll-reveal="enter from the left after 0.2s" class="col-sm-4 col-sm-offset-2">
                <span class="fui-upload"> </span>
                <h6>Прогнозирование бюджета</h6>

                <p>Планы годовой подписки для творческих рабочих групп — это удобный способ получить доступ ко всем инструментам Creative Cloud или к отдельному приложению по единой умеренной цене. Это позволяет прогнозировать затраты. Обновление всех компонентов производится бесплатно.</p>
            </div>
            <div data-scroll-reveal="enter from the right after 0.2s" class="col-sm-4 col-sm-offset-1">
                <span class="fui-credit-card"> </span>
                <h6>Администрирование</h6>

                <p>Интуитивно понятная консоль администратора позволит вам легко добавлять, отслеживать и назначать рабочие места в пределах организации, а также управлять версиями. С помощью приложения Creative Cloud Packager выполняется централизованное развертывание и обновление всех или отдельных приложений Creative Cloud.</p>
            </div>
        </div>
        <div class="row features">
            <div data-scroll-reveal="enter from the left after 0.2s" class="col-sm-4 col-sm-offset-2">
                <span class="fui-star-2"> </span>
                <h6>Обучение</h6>

                <p>Осваивайте приложения без труда благодаря библиотеке учебных пособий и видеоматериалов. Кроме того, подписчики могут воспользоваться эксклюзивными консультациями службы поддержки Adobe. </p>
            </div>
            
        </div>
    </div>
</section>
<section style="margin-top:100px; padding-top:30px;" class="projects-1" id="3">
    <div class="container">
        <!-- <div class="title">
            <a href="#">lorem ipsum</a>
            <a href="#" class="active">Lorem ipsum.</a>
            <a href="#">Lorem ipsum</a>
            <a href="#">Lorem ipsum</a>
            <a href="#">Lorem ipsum</a>
        </div> -->

        <!-- <div class="head-box">
            <div class="brand"><img src="startup/common-files/icons/logo-2@2x.png" width="42" height="21" alt=""> Приложения Adobe Creative Cloud</div>
            <p>Состав пакета Creative Cloud для рабочих групп. Выберите свое приложение или используйте сразу все!</p>

            <div class="features">
                <div>
                    <span class="fui-mic"></span> Lorem ipsum dolor.
                </div>
                <div>
                    <span class="fui-cmd"></span> Lorem ipsum dolor.
                </div>
            </div>
        </div> -->
        <!-- <img src="static/images/icecream.png" width="270" height="340" class="img-responsive center-block" alt=""> -->
        <div class="col-sm-10 col-sm-offset-1" style="text-align:center;" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
            <h3 class="lead">Приложения Adobe Creative Cloud</h3>
            <p class="lead">Состав пакета Creative Cloud для рабочих групп. Выберите свое приложение или используйте сразу все!</p>
            <br><br>
        </div>

        <div class="projects">
            <div class="project-wrapper">
                <div data-scroll-reveal="enter from the left after 0.2s" class="project" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/1.png" alt="" style="height:200px;"></div>
                        
                    </div>
                    <div class="info">
                        <div class="name">Adobe Audition CC</div>
                        <b>Adobe Audition CC</b> позволит Вам создавать и редактировать аудиоматериалы, а также улучшать их качества для видео-, теле- и кинопроектов.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div data-scroll-reveal="enter from the bottom after 0.2s" class="project" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/2.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Bridge CC</div>
                        <b>Adobe Bridge CC</b> централизует все функции обзора, систематизации и поиска Ваших фотографий и других файлов.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div data-scroll-reveal="enter from the right after 0.2s" class="project" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/3.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Dreamweaver CC</div>
                        <b>Дизайн,</b> разработка и сопровождение веб-сайтов и приложений на базе Ваших отраслевых стандартов.
                    </div>
                </div>
            </div>

        </div>
        <div class="projects">
            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the left after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/4.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Encore CC</div>
                        <b>Профессиональное приложение</b> для создания DVD-Video и Blu-Ray-дисков, поддерживающее любой формат.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the bottom after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/5.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Fireworks CC</div>
                        <b>Вы</b> сможете быстро разработать прототипы веб-сайтов и приложений, а также оптимизировать веб-графику.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the right after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/6.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Flash Professional CC</div>
                        <b>Adobe Flash Professional CC</b> поможет Вам создавать многофункциональные интерактивные материалы для различных платформ и устройств.
                    </div>
                </div>
            </div>
            
        </div>
        <div class="projects">
            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the left after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/7.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Illustrator CC</div>
                        <b>Создание</b> векторной графики для печати, интернет-сайтов, видеороликов и мобильных устройств.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the bottom after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/8.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe InCopy CC</div>
                        <b>Организует для Вас</b> одновременную работу составителей текстов и дизайнеров над одним и тем же документом.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the right after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/9.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe InDesign CC</div>
                        <b>Профессиональный</b> дизайн макетов для печати и электронных публикаций.
                    </div>
                </div>
            </div>
        </div>
        <div class="projects">
            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the left after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/10.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Photoshop CC</div>
                        <b>Adobe Photoshop CC</b> позволит Вам редактировать и составлять изображения, а также предоставит широкий набор инструментов для работы с трехмерными объектами, редактирования видео и многостороннего анализа изображений.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the bottm after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/11.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Photoshop Lightroom 5</div>
                        <b>Систематизация,</b> редактирование и пакетная обработка всех цифровых фотографий в единой библиотеке с интуитивно понятным пользовательским интерфейсом.
                    </div>
                </div>
            </div>

            <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the right after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/12.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Prelude CC</div>
                        <b>Упрощенный</b> импорт и подготовка видеоматериалов любого формата.
                    </div>
                </div>
            </div>
        </div>
        <div class="projects">
            <div class="project-wrapper" style="text-align:center;">
                <div class="project" data-scroll-reveal="enter from the left after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/13.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Premiere Pro CC</div>
                        <b>Обработка</b> видео с помощью передовых высокопроизводительных инструментов монтажа.
                    </div>
                </div>
            </div>

            <div class="project-wrapper" style="text-align:center;">
                <div class="project" data-scroll-reveal="enter from the right after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/14.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe SpeedGrade CC</div>
                        <b>Adobe SpeedGrade CC</b> позволит Вам вносить изменение освещенности и цветов в отснятом видеоматериале.
                    </div>
                </div>
            </div>

            <!-- <div class="project-wrapper">
                <div class="project" data-scroll-reveal="enter from the left after 0.2s" style="vertical-align:top;">
                    <div class="photo-wrapper">
                        <div class="photo" style="background-size:cover;"><img src="static/images/1.png" alt="" style="height:200px;"></div>
                    </div>
                    <div class="info">
                        <div class="name">Adobe Creative Cloud for teams Complete</div>
                        <b>Самый полный</b> пакет Creative Cloud для рабочих групп, предназначенный для малых и средних предприятий, даст возможность Вам и Вашей команде пользоваться последними достижениями технологий для творчества.
                    </div>
                </div>
            </div> -->
            
        </div>
    </div>
    <!--/.container-->
</section>
<section style="margin-top:100px; padding-top:30px;" class="price-2" id="4">
    <div class="container">
        <div class="plans">
            <div class="plan">
                <div class="title" >
                    <span data-scroll-reveal="after .2s, ease-in 32px and reset over .66s"> Бесплатная пробная версия <br> на месяц </span>
                    <div data-scroll-reveal="after .4s, ease-in 32px and reset over .66s" class="price" style="font-size:55px;"><!-- <span class="currency">₸</span> -->0<span class="period">₸ / Месяц</span></div>
                    <a data-scroll-reveal="after .6s, ease-in 32px and reset over .66s" class="btn btn-clear btn-small" href="#5">Попробовать бесплатно</a>
                </div>

                <div class="description">
                    <!-- <div class="description-box">
                        <span class="fui-cmd"></span> Лорем
                    </div> -->
                    <div class="description-box" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
                        <span class="fui-time"></span> <b>Срок</b> 30 дней
                    </div>
                   <!--  <div class="description-box">
                        <span class="fui-user"></span> <b>lorem</b> ipsum
                    </div> -->
                    <div class="description-box" data-scroll-reveal="after .4s, ease-in 32px and reset over .66s">
                        <span class="fui-windows"></span> <b>Хранилище</b> 2 Гигабайта
                    </div>
                </div>
            </div>

            <div class="plan plan-2">
                <div class="title">
                    <span data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">Одно приложение <br> СС Single App </span>
                    <div data-scroll-reveal="after .4s, ease-in 32px and reset over .66s" class="price" style="font-size:55px;"><!-- <span class="currency">₸</span> -->4 800*<span class="period">₸ / Месяц </span></div>
                    <a data-scroll-reveal="after .6s, ease-in 32px and reset over .66s" class="btn btn-clear btn-small" href="#5">Попробовать бесплатно</a>
                </div>

                <div class="description">
                    <div class="description-box" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
                        <span class="fui-calendar"></span> *Цена указана с учетом <br>скидки 30% до 31 Августа, в т.ч. НДС  
                    </div>
                    <div class="description-box" data-scroll-reveal="after .4s, ease-in 32px and reset over .66s">
                        <span class="fui-time"></span> <b>Срок</b> 365 дней + 1 пробный месяц
                    </div>
                    <!-- <div class="description-box">
                        <span class="fui-user"></span> <b>lorem</b> ipsum
                    </div> -->
                    <div class="description-box" data-scroll-reveal="after .6s, ease-in 32px and reset over .66s">
                        <span class="fui-windows"></span> <b>Хранилище</b> 20 Гигабайт
                    </div>
                </div>
            </div>

            <div class="plan">
                <div class="title">
                    <span data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">Creative Cloud Complete <br> Полный пакет приложений Adobe </span>
                    <div data-scroll-reveal="after .4s, ease-in 32px and reset over .66s" class="price" style="font-size:55px;"><!-- <span class="currency">₸</span> -->12 000*<span class="period">₸ / Месяц </span></div>
                    <a data-scroll-reveal="after .6s, ease-in 32px and reset over .66s" class="btn btn-clear btn-small" href="#5">Попробовать бесплатно</a>
                </div>

                <div class="description">
                    <div class="description-box" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
                        <span class="fui-calendar"></span> *Цена указана с учетом <br>скидки 30% до 31 Августа, в т.ч. НДС  
                    </div>
                    <div class="description-box" data-scroll-reveal="after .4s, ease-in 32px and reset over .66s">
                        <span class="fui-time"></span> <b>Срок</b> 365 дней + 1 пробный месяц
                    </div>
                    <!-- <div class="description-box" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
                        <span class="fui-user"></span> <b>lorem</b> ipsum
                    </div> -->
                    <div class="description-box" data-scroll-reveal="after .6s, ease-in 32px and reset over .66s">
                        <span class="fui-windows"></span> <b>Хранилище</b> 100 Гигабайт
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.container-->
</section>

<section style="margin-top:100px; padding-top:30px;" class="contacts-2" id="5">
    <div class="container">
        <div class="row" data-scroll-reveal="after .2s, ease-in 32px and reset over .66s">
            <div class="col-sm-8">
                <h3>Два плана подписки. Скидка 30% <br> Выберите свой план. Получите гарантию низкой цены на три года.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6" data-scroll-reveal="enter from the left after 0.4s">
                <form>
                    <label class="h6">ФИО / Наименование Организации</label>
                    <input type="text" class="form-control">
                    <label class="h6">E-mail</label>
                    <input type="text" class="form-control">
                    <label class="h6">Контактный Телефон</label>
                    <input type="text" class="form-control">
                    <label class="h6">Сообщение</label>
                    <textarea rows="7" class="form-control"></textarea>
                    <button type="submit" onClick="SubmitForm()" class="btn btn-primary"><span class="fui-mail"></span></button>
                </form>
            </div>

            <div class="col-sm-5 col-sm-offset-1" data-scroll-reveal="enter from the right after 0.4s">
                <h6>Как нас найти</h6>
                <p>Мы находимся по адресу г.Алматы, пр.Достык 52/2, г-на Казахстан 15 этаж, кабинет 1512</p>
                <div class="map">
                    <!--map-->
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5812.67294333877!2d76.95474821451023!3d43.24436954602443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836ef30f7e0925%3A0x8737d05d48ac0cf5!2z0prQsNC30LDSm9GB0YLQsNC9INKb0L7QvdCw0psg0q_QuQ!5e0!3m2!1sen!2s!4v1404390411310" width="400" height="300" frameborder="0" style="border:0"></iframe>
                </div>
                <h6>Звоните и пишите нам</h6>
                <p></p>
                <div class="links">
                    <a href="#"><span class="fui-phone"></span> +7 (727) 327-47-51</a><br>
                    <a href="#"><span class="fui-mail"></span> adobe@v3na.com</a>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer-1 bg-midnight-blue" style="padding-bottom:0px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-5" style="text-align:center;">
                <b>ALMA Cloud</b> Официальный Certified reseller Adobe Creative Cloud в Казахстане по программе Value Incentive Plan
                <br><br>
                <img src="static/images/certified.png" width="500" height="126" alt="">
                <br><br>
                <!-- <div class="social-btns">
                     <a href="#" class="social-btn-facebook" data-text="Startup Design Framework - http://v3na.com/about/ Используйте Adobe!" data-url="http://v3na.com/about/">
                        <div class="fui-facebook"></div>
                        <div class="fui-facebook"></div>
                    </a>
                    <a href="#" class="social-btn-twitter" data-text="Startup Design Framework - http://v3na.com/about/ Используйте Adobe!" data-url="http://v3na.com/about/">
                        <div class="fui-twitter"></div>
                        <div class="fui-twitter"></div>
                    </a> 
                </div> -->
            </div>
            <nav>
                <div class="col-sm-2 col-sm-offset-1" >
                    <h6>Меню</h6>
                    <ul>
                        <!-- <li><a href="#1">Lorem</a></li> -->
                        <li><a href="#2">Преимущества</a></li>
                        <li><a href="#3">Продукты</a></li>
                        <li><a href="#4">Цены</a></li>
                        <li><a href="#5">Контакты</a></li>
                    </ul>
                </div>
                <div class="col-sm-2" style="text-align:center;">
                    <a class="btn btn-danger btn-block" href="#5">Отправить заявку</a>
                    <br>
                    <a href="http://v3na.com"><img src="static/images/madeby.svg" width="135" height="24" alt=""></a>
                    <br>
                    <div class="fb-share-button" data-href="http://adobe.v3na.com"></div>
                </div>
            </nav>
            
                
           
        </div>
        <div class="row">
            <div style="float:right;">
                <a class="brand" href="#"><img src="static/images/adobe_down.png" width="70" height="118" alt=""></a>
            </div>
        </div>
    </div>
</footer>            <!-- Placed at the end of the document so the pages load faster -->
            <script src="startup/common-files/js/jquery-1.10.2.min.js"></script>
            <script src="startup/common-files/js/jquery.bxslider.min.js"></script>
            <script src="startup/common-files/js/jquery.scrollTo-1.4.3.1-min.js"></script>
            <script src="startup/common-files/js/jquery.sharrre.min.js"></script>
            <script src="startup/flat-ui/js/bootstrap.min.js"></script>
            <script src="startup/common-files/js/masonry.pkgd.min.js"></script>
            <script src="startup/common-files/js/modernizr.custom.js"></script>
            <script src="startup/common-files/js/page-transitions.js"></script>
            <script src="startup/common-files/js/easing.min.js"></script>
            <script src="startup/common-files/js/jquery.svg.js"></script>
            <script src="startup/common-files/js/jquery.svganim.js"></script>
            <script src="startup/common-files/js/jquery.backgroundvideo.min.js"></script>
            <script src="startup/common-files/js/froogaloop.min.js"></script>
            <script src="startup/common-files/js/startup-kit.js"></script>
            <!--<script src="static/js/jquery.countdown.js"></script>-->

            <script src="static/js/vendor/modernizr-2.6.2.min.js"></script>
            <!--<script src="http://code.jquery.com/jquery-1.10.2.js"></script>-->
            <script src="static/js/vendor/jquery.counteverest.min.js"></script>
            <script src="static/js/vendor/jquery.liquidiframe.js"></script>
            <script src="static/js/main.js"></script>
            
        </div>
    <script>
    $(function() {
      $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });
    </script>    
    <!--<script>
        window.jQuery(function ($) {
            "use strict";

            $('time').countDown({
                with_separators: true
            });
            $('.alt-1').countDown({
                css_class: 'countdown-alt-1'
            });
            $('.alt-2').countDown({
                css_class: 'countdown-alt-2'
            });

        });
    </script>-->
    <script src="scrollReveal.js/scrollReveal.js-master/scrollReveal.js"></script>
    <script>
        window.scrollReveal = new scrollReveal();
    </script>
    <script>
        function SubmitForm(){
            alert("Your form has been submitted");
        }
    </script>

    </body>
</html>
